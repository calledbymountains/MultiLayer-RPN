import tensorflow as tf
import random


## Agumentatation of data by colors.
def rand_contrast(image):
    return tf.image.random_contrast(image, lower=0.5, upper=1.5)


def rand_saturation(image):
    return tf.image.random_saturation(image, lower=0.5, upper=1.5)


def rand_hue(image):
    return tf.image.random_hue(image, max_delta=0.2)


def rand_brightness(image):
    return tf.image.random_brightness(image, max_delta=32. / 255.)


def coloraug_fn(image):
    functions = [rand_brightness, rand_contrast]
    random.shuffle(functions)
    for func in functions:
        image = func(image)
    return image


## Preprocessing functions
def normalized_image(image):
    # Rescale from [0, 255] to [0, 2]
    image = tf.multiply(image, 1. / 127.5)
    # Rescale to [-1, 1]
    return tf.subtract(image, 1.0)


def resize_small(image, height, width):
    height = tf.cast(height, dtype=tf.float32)
    width = tf.cast(width, dtype=tf.float32)
    aspect = tf.divide(height, width)
    newdim = tf.cond(tf.less_equal(height, width), lambda: (tf.constant(600, dtype=tf.int32),
                                                            tf.cast(tf.floor(tf.divide(600.0, aspect)),
                                                                    dtype=tf.int32)),
                     lambda: (
                         tf.cast(tf.floor(tf.multiply(600., aspect)), dtype=tf.int32),
                         tf.constant(600, dtype=tf.int32)))

    image = tf.expand_dims(image, 0)
    image = tf.image.resize_bilinear(image, newdim)
    image = tf.squeeze(image)
    return image


## not use
def central_crop(image):
    imgsize = tf.shape(image)
    total_crop_height = imgsize[0] - 299
    crop_top = total_crop_height // 2
    total_crop_width = imgsize[1] - 299
    crop_left = total_crop_width // 2
    crop = tf.slice(image, [crop_top, crop_left, 0], [299, 299, 3])
    return crop


def resize_bboxes(xmin, ymin, xmax, ymax, oldheight, oldwidth, newheight, newwidth):
    xmin = xmin / tf.cast(oldwidth, tf.float32)
    ymin = ymin / tf.cast(oldheight, tf.float32)
    xmax = xmax / tf.cast(oldwidth, tf.float32)
    ymax = ymax / tf.cast(oldheight, tf.float32)
    xmin = tf.to_int32(xmin * tf.cast(newwidth, tf.float32))
    ymin = tf.to_int32(ymin * tf.cast(newheight, tf.float32))
    xmax = tf.to_int32(xmax * tf.cast(newwidth, tf.float32))
    ymax = tf.to_int32(ymax * tf.cast(newheight, tf.float32))
    return xmin, ymin, xmax, ymax


def parse_proto(example_serialized):
    """
    [Q] explain more about this???
    :param example_serialized:
    :return:
    """
    with tf.device('/cpu:0'):
        feature_map = {
            'image/filename': tf.FixedLenFeature([], tf.string, default_value=''),
            'image/encoded': tf.FixedLenFeature([], tf.string, default_value=''),
            'image/height': tf.FixedLenFeature([], tf.int64, default_value=-1),
            'image/width': tf.FixedLenFeature([], tf.int64, default_value=-1),
            'image/object/bbox/xmin': tf.VarLenFeature(dtype=tf.float32),
            'image/object/bbox/ymin': tf.VarLenFeature(dtype=tf.float32),
            'image/object/bbox/xmax': tf.VarLenFeature(dtype=tf.float32),
            'image/object/bbox/ymax': tf.VarLenFeature(dtype=tf.float32),
            'image/object/bbox/label': tf.VarLenFeature(dtype=tf.int64),
            'image/object/bbox/count': tf.VarLenFeature(dtype=tf.int64),
        }

        features = tf.parse_single_example(example_serialized, feature_map)
    return features


def preprocess_test(data):
    with tf.device('/cpu:0'):
        data = parse_proto(data)
        height = data['image/height']
        width = data['image/width']
        image = data['image/encoded']
        filename = data['image/filename']
        xmin = tf.sparse_tensor_to_dense(data['image/object/bbox/xmin'], default_value=0)
        ymin = tf.sparse_tensor_to_dense(data['image/object/bbox/ymin'], default_value=0)
        xmax = tf.sparse_tensor_to_dense(data['image/object/bbox/xmax'], default_value=0)
        ymax = tf.sparse_tensor_to_dense(data['image/object/bbox/ymax'], default_value=0)
        labels = tf.sparse_tensor_to_dense(data['image/object/bbox/label'], default_value=0)
        labels = tf.cast(labels, tf.int32)
        # Image
        image = tf.image.decode_image(image)
        image = tf.cond(tf.equal(tf.shape(image)[2], 3), lambda: image, lambda: tf.stack([image, image, image]))
        image = tf.reshape(image, tf.stack([height, width, 3]))
        image = resize_small(image, height, width)
        newshape = tf.shape(image)
        xmin, ymin, xmax, ymax = resize_bboxes(xmin, ymin, xmax, ymax, height, width, newshape[0], newshape[1])
        gt = tf.stack([xmin, ymin, xmax, ymax, labels], axis=1)
        image = tf.to_float(image)
        image = tf.divide(image, tf.constant(255.0, dtype=tf.float32))
        # image = (image - 0.5)/2.0
        image.set_shape((None, None, 3))
    return image, gt, filename


def preprocess_train(data):
    with tf.device('/cpu:0'):
        data = parse_proto(data)
        height = data['image/height']
        width = data['image/width']
        image = data['image/encoded']
        filename = data['image/filename']
        xmin = tf.sparse_tensor_to_dense(data['image/object/bbox/xmin'], default_value=0)
        ymin = tf.sparse_tensor_to_dense(data['image/object/bbox/ymin'], default_value=0)
        xmax = tf.sparse_tensor_to_dense(data['image/object/bbox/xmax'], default_value=0)
        ymax = tf.sparse_tensor_to_dense(data['image/object/bbox/ymax'], default_value=0)
        labels = tf.sparse_tensor_to_dense(data['image/object/bbox/label'], default_value=0)
        labels = tf.cast(labels, tf.int32)
        randnum = tf.random_uniform([], minval=0, maxval=2, dtype=tf.int32, seed=None, name=None)
        docolor = tf.random_uniform([], minval=0, maxval=2, dtype=tf.int32, seed=None, name=None)


        # Image
        image = tf.image.decode_image(image)
        image = tf.cond(tf.equal(tf.shape(image)[2], 3), lambda: image, lambda: tf.stack([image, image, image]))
        image = tf.reshape(image, tf.stack([height, width, 3]))
        image = resize_small(image, height, width)
        newshape = tf.shape(image)
        xmin, ymin, xmax, ymax = resize_bboxes(xmin, ymin, xmax, ymax, height, width, newshape[0], newshape[1])
        gt = tf.stack([xmin, ymin, xmax, ymax, labels], axis=1)

        image, gt = tf.cond(tf.equal(randnum, tf.constant(1, dtype=tf.int32)), lambda: flip_img_bboxes(image, gt, True),
                      lambda: flip_img_bboxes(image, gt, False))

        image = tf.cond(tf.equal(docolor, tf.constant(1, dtype=tf.int32)), lambda: coloraug_fn(image), lambda : image)

        image = tf.to_float(image)
        image = tf.divide(image, tf.constant(255.0, dtype=tf.float32))
        # image = (image - 0.5)/2.0
        image.set_shape((None, None, 3))
    return image, gt, filename


def flip_img_bboxes(img, gt, doflip=False):
    if doflip:
        img = tf.image.flip_left_right(img)
        imgshape = tf.shape(img)
        xmin, ymin, xmax, ymax, label = tf.unstack(gt, axis=1)
        new_xmin = imgshape[1] - xmax - 1
        new_ymin = ymin
        new_xmax = new_xmin + (xmax - xmin)
        new_ymax = ymax
        gt = tf.stack([new_xmin, new_ymin, new_xmax, new_ymax], axis=1)
    return img, gt


def flip(img, mask, cent, flip=False):
    if flip:
        img = tf.image.flip_left_right(img)
        mask = tf.image.flip_left_right(mask)
        cent = tf.image.flip_left_right(cent)
    return img, mask, cent
