import multiprocessing as mp
import tensorflow as tf
from data.preprocessing import preprocess_train, preprocess_test


def traindb(filelist, config):
    db = tf.data.TFRecordDataset(filenames=filelist)
    if config['data']['train']['shuffle']:
        db = db.shuffle(buffer_size=1000)

    db = db.map(preprocess_train, num_parallel_calls=mp.cpu_count())

    #db =db.batch(1)
    ## prepare data and save time.
    db = db.prefetch(buffer_size=config['data']['train']['prefetch_count'])

    return db


def testdb(filelist, config):
    db = tf.data.TFRecordDataset(filenames=filelist)
    if config['data']['test']['shuffle']:
        db = db.shuffle(buffer_size=1000)

    db = db.map(preprocess_test, num_parallel_calls=mp.cpu_count())

    #db = db.batch(1)
    ## prepare data and save time.
    db = db.prefetch(buffer_size=config['data']['test']['prefetch_count'])

    return db