"""
Example usage :

# Following is for caltech reasonable (training set)
python make_query.py --query '''{"$match" : {"caltechsubset" :
 "10x-train", "anntype" : "new","occlusion" : {"$lte" : 0.35}, "height" :
  {"$gte": 50}, "object" : "person"} },{"$group" : { "_id": "$image",
  "information" : { "$push" : {"object" : "$object", "gt" : "$gtvis"}}}}'''

# To get testing set of caltech reasonable : Replace 10x-train with 1x-test



"""

import argparse
import ast

from PIL import Image
from pymongo import MongoClient

import csv

import xml.etree.ElementTree as ET

parser = argparse.ArgumentParser()
parser.add_argument('--query')
parser.add_argument('--classmap')


def make_query(query):
    results = []
    client = MongoClient('nef-devel2.inria.fr', 27017)
    db = client.pedestrians
    collections = db.collection_names(include_system_collections=False)
    for collection in collections:
        if collection == 'all':
            continue
        coll = db[collection]
        arg_agg = ast.literal_eval('''{}'''.format(query))
        resultcur = coll.aggregate([arg_agg[0], arg_agg[1]])
        resultcur = list(resultcur)
        print(len(resultcur))
        results += resultcur
    return results


def transform4tfr(results, classdict, format='JPEG'):
    for ind in range(len(results)):
        results[ind]['filename'] = results[ind].pop('_id')
        img = cv2.imread(results[ind]['filename'])
        height, width, _ = img.shape
        results[ind]['height'] = height
        results[ind]['width'] = width
        results[ind]['format'] = format
        results[ind]['id'] = ind
        results[ind]['object'] = {'bbox': {'text': [], 'label': [],
                                           'xmin': [], 'xmax': [],
                                           'ymin': [], 'ymax': []}}
        for item in results[ind]['information']:
            results[ind]['object']['bbox']['text'].append(item['object'])
            results[ind]['object']['bbox']['label'].append(classdict[item['object']])
            results[ind]['object']['bbox']['xmin'].append(item['gt'][0])
            results[ind]['object']['bbox']['xmax'].append(item['gt'][2])
            results[ind]['object']['bbox']['ymin'].append(item['gt'][1])
            results[ind]['object']['bbox']['ymax'].append(item['gt'][3])
        results[ind].pop('information')
    return results

def pascal_voc_anno_to_tfrecord(path_xml, ind, image_path, format = 'JPEG'):
    tree = ET.parse(path_xml)
    root = tree.getroot()
    ## see the structure of annotations file to understand why i do this way
    record = {}
    for subelement in root:
        if subelement.tag == 'filename':
            full_fn = image_path + '/' + subelement.text
            record['filename'] = full_fn
            img = Image.open(full_fn)
            width, height = img.size
            record['height'] = height
            record['width'] = width
            record['format'] = format
            record['id'] = ind
            record['object'] = {'bbox': {'text': [], 'label': [],
                                         'xmin': [], 'xmax': [],
                                         'ymin': [], 'ymax': []}}
        elif subelement.tag == 'object' and subelement[0].tag == 'name' \
                and subelement[0].text == 'person':

            for sse in subelement:
                if sse.tag == 'bndbox':
                    xmax, xmin, ymax, ymin = 0, 0, 0, 0
                    for num in sse:
                        if num.tag == 'xmax':
                            xmax = int(num.text)
                        elif num.tag == 'xmin':
                            xmin = int(num.text)
                        elif num.tag == 'ymin':
                            ymin = int(num.text)
                        elif num.tag == 'ymax':
                            ymax = int(num.text)
                    record['object']['bbox']['text'].append('person')
                    record['object']['bbox']['label'].append(1)
                    record['object']['bbox']['xmin'].append(xmin)
                    record['object']['bbox']['xmax'].append(xmax)
                    record['object']['bbox']['ymin'].append(ymin)
                    record['object']['bbox']['ymax'].append(ymax)
    return record

def pascal_voc_tf_record_from_xml(dataset_path='/data/stars/share/STARSDATASETS/VOCdevkit/VOC2012/',indicator = 'train'):
    """
    :param dataset_path: where you store the pascal voc dataset, we assume that this folder contains the structure as
        original dataset, so there is no safe-check here.
        For reference, this folder should contains this strcuture:
           Annotations  ImageSets  JPEGImages  labels  SegmentationClass  SegmentationObject
    :param indicator: 0 means only train, 1 means test, 2 means both of them
    :return:
    """
    assert indicator in ['train', 'val', 'trainval'], \
        'indicator of pascal_voc_tf_record_from_xml must be train, val or trainval'
    full_path = dataset_path + 'ImageSets/Main/person_' + indicator + '.txt'
    full_list_images = []
    image_path = dataset_path + "/JPEGImages/"
    with open(full_path) as csvfile:
        for line in csvfile:
            temp = line.split()
            if temp[-1] == '1':
                full_list_images.append(temp[0])
    results = []
    for ind, file_name in enumerate(full_list_images):
        path_xml = dataset_path + 'Annotations/' + file_name + '.xml'
        results.append(pascal_voc_anno_to_tfrecord(path_xml, ind, image_path))
    return results

if __name__ == "__main__":
    #args = parser.parse_args()
    #query = args.query
    #classmap = args.classmap
    #classmap = ast.literal_eval(classmap)
    #results = make_query(query)
    #results = transform4tfr(results, classmap)
    results = pascal_voc_tf_record_from_xml()
    print(results[0])
