########################################################################
## INRIA STARS 2018
########################################################################

"""
 Training MultipleRPN Model

  Author: Hung NGUYEN Thanh

"""
import argparse
from multiplerpn import MultipleRPN
import yaml
import tensorflow as tf
import logging
from colorama import init, Fore, Style
from nets.utils.image_vis import image_vis_summaries
import os
import sys

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

init()

def main(**unused):
    ########################################################

    parser = argparse.ArgumentParser(description="Training Multiple RPN model")
    parser.add_argument('--mode', choices=['train', 'test'], default='train', help="what do you want: \
                                                                                    train or test?")
    parser.add_argument("--job_name", help="path of config YAML file you will use to create the RPN")
    parser.add_argument("--from_scratch", help="If training from scratch", action="store_false")
    parser.add_argument("--display_step", type=int, help="Number of iterations after which to display summary",
                        default=100)
    parser.add_argument("--num_epochs", help="number of epochs to run", default=80, type=int)
    parser.add_argument("--model", help="path of checkpoint you would like to finetune",\
                        default='/home/hunnguye/summer2018/MultiLayerRPN/jobs/1RPN_VOC/models')
    parser.add_argument('--jobs_path', help='Path where to store the model files and the TensorBoard logs')
    parser.add_argument('--atrous', type=int, default=1)
    parser.add_argument('--config_path', help='Path to the YAML file')
    args = parser.parse_args()
    os.makedirs(os.path.join(args.jobs_path, args.job_name), exist_ok=True)
    save_path = os.path.join(args.jobs_path, args.job_name, 'models')
    tensorboard_path = os.path.join(args.jobs_path, args.job_name, 'tboard')
    from_scratch = args.from_scratch
    display_step = args.display_step
    config_path = args.config_path
    atrous = args.atrous
    stream = open(config_path, "r")
    config = yaml.load(stream)
    tf.reset_default_graph()
    mRPN = MultipleRPN(config, usetfr=True, atrous=atrous)
    if not from_scratch:
        mRPN.train()
    impvars = set(tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=config['basenet']['scope']) + tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope='RPN')) - set(mRPN.optvars)
    impvars= list(impvars)
    model_saver = tf.train.Saver(var_list=impvars)
    saver_base = tf.train.Saver(
        var_list=tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=config['basenet']['scope']))
    if from_scratch:
        mRPN.train()

    merged_summaries_scalar = tf.summary.merge_all(key='rpn')
    all_summaries = tf.summary.merge([merged_summaries_scalar, mRPN._summaries])
    init_op = tf.global_variables_initializer()
    counter = 0
    epoch = 0
    with tf.Session() as sess:
        sess.run(init_op)
        writer = tf.summary.FileWriter(tensorboard_path, sess.graph)
        if from_scratch:
            saver_base.restore(sess, '/data/stars/user/uujjwal/PhD-Projects/MultiLayerRPN/pretrained/vgg_16.ckpt')
        else:
            model_saver.restore(sess, tf.train.latest_checkpoint(args.model))
        sess.run(mRPN.trainiter.initializer)
        #sess.run(mRPN.testiter.initializer)
        while epoch < args.num_epochs:
            try:
                counter+=1
                if counter == 1 or counter % display_step == 0:
                    preddict, image, bboxes,\
                    loss, _, summaries = sess.run([mRPN._rpn_predictions, mRPN.data[0], mRPN.data[1],
                                                                            mRPN.total_loss, mRPN.train_op, all_summaries], feed_dict={mRPN.mode: True})
                    logging.info(Fore.BLUE + Style.BRIGHT + 'Epoch = {} : Iteration = {} : Loss = {}'.format(epoch + 1, counter,
                                                                           ','.join(list(map(str, loss)))))
                    writer.add_summary(summaries, global_step=counter)
                    for i in range(len(preddict)):
                        visuals = image_vis_summaries(preddict[i], config, image_visualization_mode='train', image=image, gt_bboxes=bboxes)
                        for item in visuals:
                            writer.add_summary(item, global_step=counter)
                    writer.flush()

                else:
                     sess.run(mRPN.train_op, feed_dict={mRPN.mode: True})
            except tf.errors.OutOfRangeError:
                sess.run(mRPN.trainiter.initializer)
                epoch += 1
                logging.info(Fore.RED + Style.BRIGHT + 'Epoch {} ended. Starting epoch {}'.format(epoch, epoch + 1))
                model_saver.save(sess, os.path.join(save_path, 'multiplerpn.ckpt'), global_step=epoch)
                logging.info(Fore.CYAN + Style.BRIGHT + 'Model saved for epoch {}'.format(epoch))

        writer.close()


if __name__ == "__main__":
    main()
