###########################################################################
## INRIA - STARS 2018
## Authors: Ujjwal, Hung NGUYEN Thanh.
###########################################################################

import glob
import os
import numpy as np
import tensorflow as tf
from nets.vgg16 import VGG16
from nets.rpn.rpn import RPN
from data.datasets import traindb, testdb
from nets.utils.anchors import generate_anchors_reference
from nets.utils.image_vis import image_vis_summaries


class MultipleRPN(object):
    """
         Experimental architecture for the purpose of detect small and occluded objects
    """

    def __init__(self, config, usetfr=False, atrous=1, image=None):
        """

        :param config: contains all infos for building MutipleRPN. Look for [TODO] to see an example.
               form of YML file.
        """
        # store to later use
        self._config = config
        self._num_anchors = self.config['rpn']['anchors']['num_anchors']
        self._anchors_base_size = self.config['rpn']['anchors']['base_size']
        self._anchor_scales = self.config['rpn']['anchors']['scales']
        self._anchor_aspect_ratios = self.config['rpn']['anchors']['aspect_ratios']
        self._anchor_reference = list(map(lambda x: generate_anchors_reference(x[0], x[1], x[2]),
                                          zip(self.anchor_base_size, self.anchor_aspect_ratios, self.anchor_scales)))
        self._num_anchors = list(map(lambda x: x.shape[0], self._anchor_reference))
        self._anchor_stride = self.config['rpn']['anchors']['stride']
        self._rpn_cls_loss_weight = config['loss']['rpn_cls_loss_weight']
        self._rpn_reg_loss_weight = config['loss']['rpn_reg_loss_weight']
        if usetfr:
            # get the tfrecord of training list.
            self._trainlist = self._getfilelist(self.config['data']['train'][
                                                    'path'])
            # get the tfrecord of testing list
            self._testlist = self._getfilelist(self.config['data']['test']['path'])

            # create dataset from TFRecord.
            self._traindb = traindb(self.trainlist, self.config)
            self._testdb = testdb(self.testlist, self.config)

            # create iterator
            self._trainiter = self.traindb.make_initializable_iterator()
            self._testiter = self.testdb.make_initializable_iterator()

            # tensor for input of train and test process
            self._traindata = self.trainiter.get_next()
            self._testdata = self.testiter.get_next()

            # feed the right data for the right mode
            '''
            self._data = tf.cond(tf.equal(self.mode, tf.constant(True)), lambda:
            self.traindata, lambda: self.testdata)
            '''
            self._data = self.traindata
            self._image = self.traindata[0]
            self._label = self.traindata[1]
        else:
            self._image = tf.placeholder(dtype=tf.float32, shape=(None, None, 3))
            self._label = None
            self._image.set_shape([None, None, 3])

        # train or test mode
        self.mode = tf.placeholder(dtype=tf.bool)

        '''
        # tensor for input of train and test process
        self._traindata = self.trainiter.get_next()
        self._testdata = self.testiter.get_next()

        # feed the right data for the right mode

        self._data = self.traindata
        '''
        # self.image_shape = tf.shape(self.data[0])[0:2]
        self.image_shape = tf.shape(self._image)[0:2]
        # base architecture
        self._basenet = VGG16(self.config, atrous)
        # [Q] why self.data[0] -> details form of self.data
        # self._basenet.build(self.data[0], self.mode)
        self._basenet.build(self._image, self.mode)

        # Multiple RPN Architecture.
        self._rpn_predictions = []

        # store the loss of rpn
        self._rpn_loss = []
        self._total_loss = []
        self._summaries = []
        #
        for index, attachmentkey in enumerate(self.basenet.attachmentkeys):
            with tf.variable_scope('RPN_{}'.format(index + 1)):
                rpn = RPN(self.numanchors[index], self.config)
                all_anchors = self._generate_anchors(tf.shape(self.basenet.endpoints[attachmentkey]))
                # rpn._build(self.basenet.endpoints[attachmentkey], all_anchors, self.image_shape, self.mode, self.data[1])
                rpn._build(self.basenet.endpoints[attachmentkey], all_anchors, self.image_shape, self.mode,
                           gt_boxes=self._label)
            rpn_pred = rpn._predict_dict
            rpn_pred['anchor_reference'] = tf.convert_to_tensor(self._anchor_reference[index])
            rpn_pred['conv_feature_map'] = self.basenet.endpoints[attachmentkey]
            '''
            summaries = image_vis_summaries(
            rpn_pred, config=self._config,
            image_visualization_mode='train',
            image=self.data[0],
            gt_bboxes=self.data[1]
            )

            self._summaries.append(summaries)
            '''

            if usetfr:
                rpn_loss = rpn.loss(rpn_pred['rpn_prediction'])
                self._rpn_predictions.append(rpn_pred)
                self._rpn_loss.append(rpn_loss)
                for ind, item in enumerate(self._rpn_loss):
                    reg_loss_vgg = tf.losses.get_regularization_losses(self._config['basenet']['scope'])
                    reg_loss_vgg = tf.reduce_sum(reg_loss_vgg) * 0.0005
                    reg_loss_rpn = tf.losses.get_regularization_losses(scope='RPN_{}'.format(ind + 1))
                    reg_loss_rpn = tf.reduce_sum(reg_loss_rpn) * 0.0005
                    totalloss = item['rpn_cls_loss'] + item['rpn_reg_loss'] + reg_loss_vgg + reg_loss_rpn
                    self._total_loss.append(totalloss)
                    loss_summary = tf.summary.scalar('total_loss_RPN_{}'.format(ind + 1), totalloss)
                    self._summaries.append(loss_summary)
            else:
                del rpn_pred['gt_boxes']
                self._rpn_predictions.append(rpn_pred)

        print(self._rpn_predictions)

        self._train_ops = []
        self._train_op = None
        self.optvars = []

    def train(self):
        """  train function to
        :param kwags:
        :return:
        """
        for ind, loss in enumerate(self._total_loss):
            opt = tf.train.AdamOptimizer()
            opt_var = opt.variables()
            self.optvars.extend(opt_var)
            # opt = tf.train.MomentumOptimizer(learning_rate=0.001, momentum=0.9)
            with tf.control_dependencies(self._train_ops[:ind]):
                update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS, scope='RPN_{}'.format(ind+1))
                with tf.control_dependencies(update_ops):
                    self._train_ops.append(opt.minimize(loss))

        self._train_op = tf.group(self._train_ops)
        return None

    def _generate_anchors(self, feature_map_shape):
        """Generate anchor for an image.
        Using the feature map, the output of the pretrained network for an
        image, and the anchor_reference generated using the anchor config
        values. We generate a list of anchors.
        Anchors are just fixed bounding boxes of different ratios and sizes
        that are uniformly generated throught the image.
        Args:
            feature_map_shape: Shape of the convolutional feature map used as
                input for the RPN. Should be (batch, height, width, depth).
        Returns:
            all_anchors: A flattened Tensor with all the anchors of shape
                `(num_anchors_per_points * feature_width * feature_height, 4)`
                using the (x1, y1, x2, y2) convention.
        """
        with tf.variable_scope('generate_anchors'):
            grid_width = feature_map_shape[2]  # width
            grid_height = feature_map_shape[1]  # height
            shift_x = tf.range(grid_width) * self._anchor_stride
            shift_y = tf.range(grid_height) * self._anchor_stride
            shift_x, shift_y = tf.meshgrid(shift_x, shift_y)

            shift_x = tf.reshape(shift_x, [-1])
            shift_y = tf.reshape(shift_y, [-1])

            shifts = tf.stack(
                [shift_x, shift_y, shift_x, shift_y],
                axis=0
            )

            shifts = tf.transpose(shifts)
            # Shifts now is a (H x W, 4) Tensor

            # Expand dims to use broadcasting sum.
            all_anchors = (
                    np.expand_dims(self._anchor_reference, axis=0) +
                    tf.expand_dims(shifts, axis=1)
            )

            # Flatten
            all_anchors = tf.reshape(
                all_anchors, (-1, 4)
            )
            return all_anchors

    @staticmethod
    def _getfilelist(path):
        files = glob.glob(os.path.join(path, '*'))
        return files

    @property
    def trainlist(self):
        return self._trainlist

    @property
    def testlist(self):
        return self._testlist

    @property
    def config(self):
        return self._config

    @property
    def traindb(self):
        return self._traindb

    @property
    def testdb(self):
        return self._testdb

    @property
    def trainiter(self):
        return self._trainiter

    @property
    def testiter(self):
        return self._testiter

    @property
    def traindata(self):
        return self._traindata

    @property
    def testdata(self):
        return self._testdata

    @property
    def data(self):
        return self._data

    @property
    def basenet(self):
        return self._basenet

    @property
    def rpns(self):
        return self._rpns

    @property
    def train_ops(self):
        return self._train_ops

    @property
    def train_op(self):
        return self._train_op

    @property
    def numanchors(self):
        return self._num_anchors

    @property
    def anchor_scales(self):
        return self._anchor_scales

    @property
    def anchor_base_size(self):
        return self._anchors_base_size

    @property
    def anchor_aspect_ratios(self):
        return self._anchor_aspect_ratios

    @property
    def total_loss(self):
        return self._total_loss