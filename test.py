import argparse
from multiplerpn import MultipleRPN
import yaml
import tensorflow as tf
import logging
from colorama import init, Fore, Style
from nets.utils.image_vis import draw_top_nms_proposals
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt

logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=logging.INFO,
    datefmt='%Y-%m-%d %H:%M:%S')

init()


def resize_small_ar(image):
    height, width, _ = image.shape
    ar = float(height) / float(width)
    if height > width:
        newwidth = 600
        newheight = int(ar * float(newwidth))
    else:
        newheight = 600
        newwidth = int(float(newheight) / ar)

    image = cv2.resize(image, (newwidth, newheight))
    return image


def main():
    parser = argparse.ArgumentParser(description="Training Multiple RPN model")
    parser.add_argument("--config_path", help="path of config YAML file you will use to create the RPN")
    parser.add_argument("--imagefile", help="Path to the image which has to be tested.")
    parser.add_argument("--modelpath", help="Path to the model")
    args = parser.parse_args()
    stream = open(args.config_path, "r")
    config = yaml.load(stream)

    imagefile = args.imagefile
    image = cv2.imread(imagefile)
    image = image[:, :, ::-1]
    print(image.shape)
    image = resize_small_ar(image)
    image = image.astype(np.float32) / 255.0
    print(image.shape)
    mRPN = MultipleRPN(config, usetfr=False)
    model_saver = tf.train.Saver()

    initop = tf.global_variables_initializer()
    with tf.Session() as sess:
        sess.run(initop)
        model_saver.restore(sess, tf.train.latest_checkpoint(args.modelpath))
        print(image)
        preddict = sess.run(mRPN._rpn_predictions, feed_dict={mRPN.mode: True, mRPN._image: image})
        print(preddict)
        for i in range(len(preddict)):
            image = draw_top_nms_proposals(preddict[i], image, min_score=0.99)

    plt.imshow(image)
    plt.show()
    plt.close()

if __name__ == "__main__":
    main()