import abc
from data.datasets import traindb, testdb


class BaseNet(abc.ABC):
    def __init__(self, config):
        self._name = config['basenet']['name']
        self._numclasses = config['basenet']['numclasses']
        self._endpoints = None
        self._attachmentkeys = None
        self._pretrained = None
        self._basescope = config['basenet']['scope']
        if config['basenet']['pretrained'] is not None:
            self._pretrained = config['basenet']['pretrained']

        self.attachmentkeys = config['basenet']['attachmentkeys']

    @property
    def name(self):
        return self._name

    @property
    def pretrained(self):
        return self._pretrained

    @property
    def numclasses(self):
        return self._numclasses

    @abc.abstractmethod
    def build(self, images, mode):
        return NotImplementedError

    @property
    def endpoints(self):
        return self._endpoints

    @property
    def validkeys(self):
        return self.endpoints.keys()

    @property
    def attachmentkeys(self):
        return self._attachmentkeys

    @attachmentkeys.setter
    def attachmentkeys(self, value):
        """ Convert the list of values in form of a list of endpoints (names of layers).
        """
        value = value.split(',')
        self._attachmentkeys = value
