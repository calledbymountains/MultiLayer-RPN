from nets.BaseNetwork import BaseNet
from nets.slim_models import vgg
import tensorflow as tf
import logging

slim = tf.contrib.slim


class VGG16(BaseNet):
    def __init__(self, config, atrous):
        super(VGG16, self).__init__(config)
        self._atrous = atrous

    def build(self, images, mode):
        with slim.arg_scope(vgg.vgg_arg_scope()):
            _, self._endpoints = vgg.vgg_16(tf.expand_dims(images, 0), num_classes=self.numclasses, rate=self._atrous, is_training=mode, scope=self._basescope)
        logging.info('Base VGG16 network created.')
