import logging

import tensorflow as tf

from nets.rpn.rpn_proposal import RPNProposal
from nets.rpn.rpn_target import RPNTarget
from nets.utils.vars import layer_summaries, variable_summaries
from utils.losses import smooth_l1_loss

class RPN(object):
    def __init__(self, num_anchors, config, seed=0, debug=True):
        self._numconv = config['rpn']['numconv']
        self.numfilters = config['rpn']['numfilters']
        self._conv = None
        self._rpn_cls = None
        self._rpn_bbox = None
        self._num_anchors = num_anchors
        self._config = config
        self._debug = debug
        self._seed = seed
        self._l1_sigma = config['loss']['l1_sigma']
        self._predict_dict = {}
        logging.info('Number of conv layers in RPN : {}'.format(self.numconv))
        logging.info('Filter sizes in RPN conv layers : {}'.format(
            config['rpn']['numfilters']))

    @property
    def numconv(self):
        return self._numconv

    @property
    def conv(self):
        return self._conv

    @property
    def num_anchors(self):
        return self._num_anchors

    @property
    def config(self):
        return self._config

    @property
    def rpn_cls(self):
        return self._rpn_cls

    @property
    def rpn_bbox(self):
        return self._rpn_bbox

    def _build(self, input, all_anchors, im_shape, mode, gt_boxes=None):
        out = None
        for i in range(self.numconv):
            if i == 0:
                out = tf.layers.conv2d(inputs=input, filters=self.numfilters[i],
                                       kernel_size=3,
                                       padding='SAME',
                                       kernel_initializer=tf.initializers.random_normal,
                                       bias_initializer=tf.initializers.random_normal,
                                       kernel_regularizer=tf.contrib.layers.l2_regularizer(
                                           scale=0.0005),
                                       activation=tf.nn.relu, trainable=True,
                                       name='rpn_conv{}'.format(i + 1))

                out = tf.layers.batch_normalization(out, momentum=0.9,
                                                    trainable=True,
                                                    training=mode,
                                                    name='rpn_bn{}'.format(i))
            else:
                out = tf.layers.conv2d(inputs=out,
                                       filters=self.numfilters[i],
                                       kernel_size=3,
                                       kernel_initializer=tf.initializers.random_normal,
                                       bias_initializer=tf.initializers.random_normal,
                                       kernel_regularizer=tf.contrib.layers.l2_regularizer(
                                           scale=0.0005),
                                       activation=tf.nn.relu, trainable=mode,
                                       name='rpn_conv{}'.format(i + 1))

                out = tf.layers.batch_normalization(out, momentum=0.9,
                                                    trainable=True,
                                                    training=mode,
                                                    name='rpn_bn{}'.format(i))

            logging.debug('RPN conv number {} created.'.format(i + 1))

        self._conv = out

        #self._predict_dict['rpn_prediction'] = self._conv
        self._predict_dict['gt_boxes'] = gt_boxes
        self._predict_dict['image'] = input
        self._predict_dict['image_shape'] = im_shape
        self._predict_dict['all_anchors'] = all_anchors

        self._rpn_cls = tf.layers.conv2d(inputs=self.conv,
                                         filters=self.num_anchors * 2,
                                         kernel_size=1,
                                         trainable=True, name='rpn_cls')

        self._rpn_bbox = tf.layers.conv2d(inputs=self.conv,
                                          filters=self.num_anchors * 4,
                                          kernel_size=1,
                                          trainable=True, name='rpn_bbox')

        self._proposal = RPNProposal(
            self._num_anchors, self.config
        )
        self._anchor_target = RPNTarget(
            self._num_anchors, self.config, seed=self._seed
        )

        prediction_dict = {}

        # Get the RPN feature using a simple conv net. Activation function
        # can be set to empty.
        rpn_feature = self.conv
        # Then we apply separate conv layers for classification and regression.
        rpn_cls_score_original = self._rpn_cls#(rpn_feature)
        rpn_bbox_pred_original = self._rpn_bbox#(rpn_feature)
        # rpn_cls_score_original has shape (1, H, W, num_anchors * 2)
        # rpn_bbox_pred_original has shape (1, H, W, num_anchors * 4)
        # where H, W are height and width of the pretrained feature map.

        # Convert (flatten) `rpn_cls_score_original` which has two scalars per
        # anchor per location to be able to apply softmax.
        rpn_cls_score = tf.reshape(rpn_cls_score_original, [-1, 2])
        # Now that `rpn_cls_score` has shape (H * W * num_anchors, 2), we apply
        # softmax to the last dim.
        rpn_cls_prob = tf.nn.softmax(rpn_cls_score)

        prediction_dict['rpn_cls_prob'] = rpn_cls_prob
        prediction_dict['rpn_cls_score'] = rpn_cls_score

        # Flatten bounding box delta prediction for easy manipulation.
        # We end up with `rpn_bbox_pred` having shape (H * W * num_anchors, 4).
        rpn_bbox_pred = tf.reshape(rpn_bbox_pred_original, [-1, 4])

        prediction_dict['rpn_bbox_pred'] = rpn_bbox_pred

        # We have to convert bbox deltas to usable bounding boxes and remove
        # redundant ones using Non Maximum Suppression (NMS).
        proposal_prediction = self._proposal._build(
            rpn_cls_prob, rpn_bbox_pred, all_anchors, im_shape)

        prediction_dict['proposals'] = proposal_prediction['proposals']
        prediction_dict['scores'] = proposal_prediction['scores']

        if self._debug:
            prediction_dict['proposal_prediction'] = proposal_prediction

        if gt_boxes is not None:
            # When training we use a separate module to calculate the target
            # values we want to output.
            (rpn_cls_target, rpn_bbox_target,
             rpn_max_overlap) = self._anchor_target(
                all_anchors, gt_boxes, im_shape
            )

            prediction_dict['rpn_cls_target'] = rpn_cls_target
            prediction_dict['rpn_bbox_target'] = rpn_bbox_target

            if self._debug:
                prediction_dict['rpn_max_overlap'] = rpn_max_overlap
                variable_summaries(rpn_bbox_target, 'rpn_bbox_target',
                                   'full')

        # Variables summaries.
        variable_summaries(prediction_dict['scores'], 'rpn_scores',
                           'reduced')
        variable_summaries(rpn_cls_prob, 'rpn_cls_prob', 'reduced')
        variable_summaries(rpn_bbox_pred, 'rpn_bbox_pred', 'reduced')

        if self._debug:
            variable_summaries(rpn_feature, 'rpn_feature', 'full')
            variable_summaries(
                rpn_cls_score_original, 'rpn_cls_score_original', 'full')
            variable_summaries(
                rpn_bbox_pred_original, 'rpn_bbox_pred_original', 'full')

            # Layer summaries.
            #layer_summaries(self._conv, 'full')
            #layer_summaries(self._rpn_cls, 'full')
            #layer_summaries(self._rpn_bbox, 'full')

        self._predict_dict['rpn_prediction'] = prediction_dict
        return None

    def loss(self, prediction_dict):
        """
        Returns cost for Region Proposal Network based on:
        Args:
            rpn_cls_score: Score for being an object or not for each anchor
                in the image. Shape: (num_anchors, 2)
            rpn_cls_target: Ground truth labeling for each anchor. Should be
                * 1: for positive labels
                * 0: for negative labels
                * -1: for labels we should ignore.
                Shape: (num_anchors, )
            rpn_bbox_target: Bounding box output delta target for rpn.
                Shape: (num_anchors, 4)
            rpn_bbox_pred: Bounding box output delta prediction for rpn.
                Shape: (num_anchors, 4)
        Returns:
            Multiloss between cls probability and bbox target.
        """
        rpn_cls_score = prediction_dict['rpn_cls_score']
        rpn_cls_target = prediction_dict['rpn_cls_target']

        rpn_bbox_target = prediction_dict['rpn_bbox_target']
        rpn_bbox_pred = prediction_dict['rpn_bbox_pred']

        with tf.variable_scope('RPNLoss'):
            # Flatten already flat Tensor for usage as boolean mask filter.
            rpn_cls_target = tf.cast(tf.reshape(
                rpn_cls_target, [-1]), tf.int32, name='rpn_cls_target')
            # Transform to boolean tensor mask for not ignored.
            labels_not_ignored = tf.not_equal(
                rpn_cls_target, -1, name='labels_not_ignored')

            # Now we only have the labels we are going to compare with the
            # cls probability.
            labels = tf.boolean_mask(rpn_cls_target, labels_not_ignored)
            cls_score = tf.boolean_mask(rpn_cls_score, labels_not_ignored)

            # We need to transform `labels` to `cls_score` shape.
            # convert [1, 0] to [[0, 1], [1, 0]] for ce with logits.
            cls_target = tf.one_hot(labels, depth=2)

            # Equivalent to log loss
            ce_per_anchor = tf.nn.softmax_cross_entropy_with_logits_v2(
                labels=cls_target, logits=cls_score
            )
            prediction_dict['cross_entropy_per_anchor'] = ce_per_anchor

            # Finally, we need to calculate the regression loss over
            # `rpn_bbox_target` and `rpn_bbox_pred`.
            # We use SmoothL1Loss.
            rpn_bbox_target = tf.reshape(rpn_bbox_target, [-1, 4])
            rpn_bbox_pred = tf.reshape(rpn_bbox_pred, [-1, 4])

            # We only care for positive labels (we ignore backgrounds since
            # we don't have any bounding box information for it).
            positive_labels = tf.equal(rpn_cls_target, 1)
            rpn_bbox_target = tf.boolean_mask(rpn_bbox_target, positive_labels)
            rpn_bbox_pred = tf.boolean_mask(rpn_bbox_pred, positive_labels)

            # We apply smooth l1 loss as described by the Fast R-CNN paper.
            reg_loss_per_anchor = smooth_l1_loss(
                rpn_bbox_pred, rpn_bbox_target, sigma=self._l1_sigma
            )

            prediction_dict['reg_loss_per_anchor'] = reg_loss_per_anchor

            # Loss summaries.
            tf.summary.scalar('batch_size', tf.shape(labels)[0], ['rpn'])
            foreground_cls_loss = tf.boolean_mask(
                ce_per_anchor, tf.equal(labels, 1))
            background_cls_loss = tf.boolean_mask(
                ce_per_anchor, tf.equal(labels, 0))
            tf.summary.scalar(
                'foreground_cls_loss',
                tf.reduce_mean(foreground_cls_loss), ['rpn'])
            tf.summary.histogram(
                'foreground_cls_loss', foreground_cls_loss, ['rpn'])
            tf.summary.scalar(
                'background_cls_loss',
                tf.reduce_mean(background_cls_loss), ['rpn'])
            tf.summary.histogram(
                'background_cls_loss', background_cls_loss, ['rpn'])
            tf.summary.scalar(
                'foreground_samples', tf.shape(rpn_bbox_target)[0], ['rpn'])

            return {
                'rpn_cls_loss': tf.reduce_mean(ce_per_anchor),
                'rpn_reg_loss': tf.reduce_mean(reg_loss_per_anchor),
            }
