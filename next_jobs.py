from pathlib import Path
import argparse
import os
from string import Template
from utils.writeyaml import create_config_file

## change source data the suffix is test or val/ validation
source_data = '/home/hunnguye/summer2018/MultiLayerRPN/datasets/pascal_voc_2012/'
#source_data = '/data/stars/user/uujjwal/PhD-Projects/MultiLayerRPN/tfrecord/reasonable/'
suffix_train_path = '/train'
suffix_test_path = '/val'

## for caltech dataset.
#


## for PAscal voc
# '/home/hunnguye/summer2018/MultiLayerRPN/datasets/pascal_voc_2012/'


## constant path of folder

jobs_path = '/data/stars/user/uujjwal/PhD-Projects/emergency/jobs/'
#jobs_path = '/home/hunnguye/summer2018/MultiLayerRPN/jobs/'
model_folder = 'models/'
tensorboard_folder = 'tboard/'
submit_script = 'submit.sh'
data_path = '/local/data_hung_'

def create_the_new_job(job_name, dstep, runtime):
    """
    - create the new folder for the job
    - generate the new config file for the job
    - replace text in submit file to new name
    :param job_name:
    :return:
    """
    ## path of folder contains data of new jobs
    new_path = jobs_path + job_name
    job_folder = Path(new_path)
    if job_folder.is_dir():
        print("Aborted: this name of jobs is already existed. Retry!!")
        return None, None
    os.makedirs(new_path)
    model_path = new_path +'/' + model_folder
    os.makedirs(model_path)
    tboard_path = new_path + '/' + tensorboard_folder
    os.makedirs(tboard_path)

    ## generate the new config path
    ##
    train_path = data_path + job_name + suffix_train_path
    test_path = data_path + job_name + suffix_test_path
    create_config_file(new_path + '/config.yml',test_path, train_path)

    ## generate the submit jobs
    with open('./templates/submit.template') as templ:
        d = {'job_name' : job_name, 'display_step' : str(dstep), 'run_time' : str(runtime), 'source_data' : source_data}
        src = Template(templ.read())
        result = src.substitute(d)
        with open(new_path + '/' + submit_script, 'w') as submit:
            submit.write(result)

    return model_path, tboard_path

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create the new jobs to generate')
    parser.add_argument('job_name', help='the name of the jobs')
    parser.add_argument('--dstep', help='the number of steps to display the info', default=100, type=int)
    parser.add_argument('--run_time', help='how long the jobs will runs', default=10, type=int)

    args = parser.parse_args()
    mpath, tbpath = create_the_new_job(args.job_name, args.dstep, args.run_time)
    if mpath is not None and tbpath is not None:
        print("Model path is created at : ", mpath)
        print("tensorboard path is created at :", tbpath)





