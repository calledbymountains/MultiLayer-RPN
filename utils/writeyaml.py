import yaml
import argparse


def create_config_file(config_path, test_path, train_path):
    data = {
        'basenet': {
            'name': 'VGG16',
            'numclasses': 1000,
            'pretrained': None,
            'attachmentkeys': 'vgg_16/conv5/conv5_3',
            'scope': 'vgg_16'
        },
        'rpn': {
            'numconv': 1,
            'numfilters': [256],
            'num_anchors': [9, 9],
            'proposals': {
                'pre_nms_top_n': 12000,
                'apply_nms': True,
                'post_nms_top_n': 2000,
                'nms_threshold': 0.7,
                'min_size': 0,
                'filter_outside_anchors': True,
                'clip_after_nms': False,
                'min_prob_threshold': 0.0,
            },
            'target': {
                'allowed_border': 0,
                'clobber_positives': False,
                'foreground_threshold': 0.7,
                'background_threshold_high': 0.3,
                'foreground_fraction': 0.5,
                'minibatch_size': 256,
            },
            'anchors': {
                'num_anchors': [20],
                'base_size': [64],
                'scales': [[0.25, 0.5, 1, 2, 4]],
                'aspect_ratios': [[1.0, 2.0, 2.43, 3]],
                'stride': [[16]]
            },

        },
        'data': {
            'train': {
                'path': train_path, #'/local/hunnguye/tfrecord/VOC12/train'
                'shuffle': True,
                'prefetch_count': 512
            },
            'test': {
                'path': test_path, # '/local/ujjwal/tfrecord/VOC12/val'
                'shuffle': False,
                'prefetch_count': 256
            }
        },
        'loss': {
            'rpn_cls_loss_weight': 1,
            'rpn_reg_loss_weight': 1,
            'l1_sigma': 3.0
        },

    }
    with open(config_path, 'w') as outfile:
        yaml.dump(data, outfile, default_flow_style=False)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='generate the config file for the tasks')
    parser.add_argument("config_path", help="path of config file you want to created")
    parser.add_argument("train_path", help="path of train data")
    parser.add_argument("test_path", help="path of test or valid data")
    args = parser.parse_args()
    create_config_file(args.config_path, args.test_path, args.train_path)


