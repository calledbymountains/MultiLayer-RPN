#!/bin/bash

#OAR -p gpu='YES' and dedicated='stars'
#OAR -l /gpunum=1,walltime=1
#OAR --name maketfr
#OAR --array-param-file tfrparams.txt

source activate tensorflow
module load cuda/9.1 cudnn/7.0-cuda-9.1
export PYTHONPATH=../../:$PYTHONPATH

echo $1
python ../../data/create_tfrecords.py --query "$1" --classmap "$2" --shards "$3" --output_dir "$4" --prefix "$5"
