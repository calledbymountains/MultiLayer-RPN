#!/usr/bin/env bash

display_step=${2:-100}
run_time=${3:-10}
job_name=${1}

echo "Setting for this commands: "
echo "Job name : ${job_name}"
echo "Running time : ${run_time}"
echo "Display step: ${display_step}"
source activate tensorflow
python next_jobs.py ${job_name} --dstep ${display_step} --run_time ${run_time}
source deactivate
chmod +x /data/stars/user/uujjwal/PhD-Projects/emergency/jobs/${job_name}/submit.sh
#/home/hunnguye/summer2018/MultiLayerRPN/jobs/${job_name}/submit.sh
oarsub -S /data/stars/user/uujjwal/PhD-Projects/emergency/jobs/${job_name}/submit.sh
